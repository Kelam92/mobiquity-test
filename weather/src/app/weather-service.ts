import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  config = "https://api.openweathermap.org/data/2.5/";
  appId = "d99029eacec18aca74102e78d3dbea59";
  key = "439d4b804bc8187953eb36d2a8c26a02"
  constructor(private http: HttpClient) { }


  getWeatherData(obj) {
    return this.http.get(`${this.config}weather?q=${obj.cityName},${obj.countryName}&units=metric&appid=${this.appId}`);
  }

  getCityWeatherInfo(city) {
    return this.http.get(`${this.config}forecast?q=${city}&units=metric&cnt=7&appid=${this.appId}`);
  }
}
