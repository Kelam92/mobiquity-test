import { ComponentFixture, fakeAsync, flush, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { of } from 'rxjs';

import { WeatherComponent } from './weather.component';
import { WeatherService } from '../weather-service';

describe('WeatherComponent', () => {
  let component: WeatherComponent;
  let fixture: ComponentFixture<WeatherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherComponent ],
      imports:[RouterTestingModule, HttpClientTestingModule],
      providers: [
        { provide: WeatherService }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should test ngOnInit', () => {
    let data = {
      name: 'London',
      main: {
        temp: 1
      },
      sys: {
        sunrise: 123456766445,
        sunset: 6374673263285
      }
    };
    spyOn(component.weatherService, 'getWeatherData').and.returnValue(of(data))
    component.ngOnInit();
    expect(component.londonInfo).toBeTruthy();
  });
});
