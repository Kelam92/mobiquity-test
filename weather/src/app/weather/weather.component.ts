import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { WeatherService } from '../weather-service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {

  cityArr = [
    {
      cityName: "London",
      countryName: "uk"
    },
    {
      cityName: "Paris",
      countryName: "france"
    },
    {
      cityName: "Rome",
      countryName: "italy"
    },
    {
      cityName: "Venice",
      countryName: "italy"
    },
    {
      cityName: "Zurich",
      countryName: "switzerland"
    }
  ];
  londonInfo = {};
  parisInfo = {};
  romeInfo = {};
  veniceInfo = {};
  zurichInfo = {};

  constructor(private router: Router, public weatherService: WeatherService) { }

  ngOnInit() {
    for (const value of this.cityArr) {
      this.weatherService.getWeatherData(value).subscribe((data) => {
        if(data['name'] === 'London') {
          this.londonInfo = {
            temperature: data['main'].temp,
            sunrise: this.convertTime(data['sys'].sunrise),
            sunset: this.convertTime(data['sys'].sunset)
          }
        } else if(data['name'] === 'Paris') {
          this.parisInfo = {
            temperature: data['main'].temp,
            sunrise: this.convertTime(data['sys'].sunrise),
            sunset: this.convertTime(data['sys'].sunset)
          }
        } else if(data['name'] === 'Rome') {
          this.romeInfo = {
            temperature: data['main'].temp,
            sunrise: this.convertTime(data['sys'].sunrise),
            sunset: this.convertTime(data['sys'].sunset)
          }
        } else if(data['name'] === 'Venice') {
          this.veniceInfo = {
            temperature: data['main'].temp,
            sunrise: this.convertTime(data['sys'].sunrise),
            sunset: this.convertTime(data['sys'].sunset)
          }
        } else {
          this.zurichInfo = {
            temperature: data['main'].temp,
            sunrise: this.convertTime(data['sys'].sunrise),
            sunset: this.convertTime(data['sys'].sunset)
          }
        }
      });
    }
  }

  private convertTime(time) {
    let date = new Date((time * 1000));
    let h = date.getUTCHours();
    let m = "0" + date.getUTCMinutes();
    let t = h + ":" + m.substr(-2);
    return t
  }

  onClick(city:string) {
    this.router.navigate(['/city', city]);
  }

}
