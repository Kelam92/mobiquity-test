import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router'
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CityWeatherComponent } from './city-weather/city-weather.component';
import { WeatherComponent } from './weather/weather.component';

export const appRoutes: Routes = [
  { path: '', component: WeatherComponent },
  { path: 'city/:cityName', component: CityWeatherComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CityWeatherComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
