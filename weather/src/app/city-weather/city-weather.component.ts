import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { WeatherService } from '../weather-service';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.css']
})
export class CityWeatherComponent implements OnInit {
  city:string;
  weatherArr;
  constructor(private route: ActivatedRoute, private weatherService: WeatherService) { }

  ngOnInit() {
    this.city = this.route.snapshot.paramMap.get('cityName');

    this.weatherService.getCityWeatherInfo(this.city).subscribe((data) => {
      this.weatherArr = data['list'];
    });
  }

}
